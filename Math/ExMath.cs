﻿/*
* AUTHOR: Angel Navarrete Sanchez
* DATE: 2022/12/01
* DESCRIPTION:  Copia aquest codi. Soluciona tots els errors i warnings, per tal que s'executi. Depura'l, pas a pas fins que arribis a una conclusió i contesta a les següents preguntes, mitjançant comentaris al final del main:
                    Quin valor té result quan la i == 1000?
                    El valor result és mai major que 110? I que 120? 
 */
using System;

namespace ExMath
{
    class ExMath
    {
        static void Main()
        {
            var result = 0.0;
            for (int i = 0; i < 10000; i++)
            {
                if (result > 100)
                {
                    result = Math.Sqrt(result);
                }

                if (result < 0)
                {
                    result += result * result;
                }

                result += 20.2;
            }

            Console.WriteLine($"el resultat és {result}");
        }
        /*
         * Quin valor té result quan la i == 1000? El valor de result quant la i val 1000 es 111.56230589874906 al principi del for i 30.762305898749055 al final.
         * El valor result és mai major que 110? I que 120? Superior a 110 si, Superior a 120 no.
        */

    }
}
